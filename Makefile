run:
	docker-compose up -d
	docker-compose ps

stop:
	docker-compose down
	docker-compose ps

restart:
	docker-compose down
	docker-compose up -d
	docker-compose ps

test:
	docker-compose exec php vendor/bin/phpunit --debug Tests

composer-install:
	docker-compose exec php composer install

composer-require:
	docker-compose exec php composer $(cmd)

composer-autoload:
	docker-compose exec php composer dump-autoload -o


