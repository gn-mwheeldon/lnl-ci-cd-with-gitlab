<?php

require_once('vendor/autoload.php');

use App\Form\Form;
use App\Calculator\Calculator;

$form = new Form();
$calculator = new Calculator();

$form->render();

if (isset($_POST['number1']) && isset($_POST['number2'])) {

    $number1 = (int)$_POST['number1'];
    $number2 = (int)$_POST['number2'];

    $result = $calculator->multiply($number1, $number2);

    $form->renderResult($number1, $number2, $result);
}
