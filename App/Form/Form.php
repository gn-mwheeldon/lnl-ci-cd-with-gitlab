<?php

namespace App\Form;

class Form
{
    public function render(): void
    {
        echo <<<EOT
        <h1>Super Calculator!!!</h1>
<form method="POST">
<input type="number" name="number1"/><br/><br/>
<input type="number" name="number2"/><br/><br/>
<input type="submit" value="Calculate"/>
</form>
EOT;
    }

    /**
     * @param int $number1
     * @param int $number2
     * @param int $result
     * @return void
     */
    public function renderResult(int $number1, int $number2, int $result): void
    {
        echo sprintf(
            "<p>The answer to %s multiplied by %s is %s.</p>",
            $number1,
            $number2,
            $result
        );
    }
}
