<?php

namespace App\Calculator;

class Calculator
{
    /**
     * @param int $number1
     * @param int $number2
     * @return int
     */
    public function multiply(int $number1, int $number2): int
    {
        return $number1 * $number2;
    }
}
