FROM php:8.1-fpm-alpine

RUN apk add --update make && \
    curl -sSL https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer && \
    cd /var/www/html

WORKDIR /var/www/html
ADD index.php index.php
ADD App App
ADD composer.json composer.json

RUN composer install

CMD php -S 0.0.0.0:8000 -t .
