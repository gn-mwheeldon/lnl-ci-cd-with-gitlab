<?php

use PHPUnit\Framework\TestCase;

use App\Calculator\Calculator;

class CalculatorTest extends TestCase
{
    public function testCalculator_With4And5_Returns20()
    {
        $calculator = new Calculator();

        $expected = 20;
        $actual = $calculator->multiply(4, 5);

        $this->assertEquals($expected, $actual);
    }
}
